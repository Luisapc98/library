import React , {Component} from 'react'
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import axios from 'axios'
import arrow from '../img/arrow.png';
import {NavLink} from 'react-router-dom';
import { DatePicker , Select  , Radio , AutoComplete , Modal} from 'antd';
const RadioGroup = Radio.Group;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const Option = Select.Option;
const OptionSecond = AutoComplete.Option;


class RegisterPrestamo extends Component {
    constructor() {
        super()
		this.state = {
        	booksAvailable : [],
			teachersAvailable : [],
			studentsAvailable : [],
			dataAvailableToSelect : [],
            bookSelected : {},
            studentSelected : null,
            valueChoose : 0,
			date_from : '',
			date_to : '',
		}

		this.onChange = this.onChange.bind(this)
		this.handleOnSubmitButton = this.handleOnSubmitButton.bind(this)
    }

    componentDidMount(){
        axios.get('http://localhost:8000/students').then((response)=>{
        	let arrayOfStudent = []
        	if(response.status === 200){
                response.data.data.map(student=>{
                	axios.get('http://localhost:8000/careers' + "/" + student.career).then(response2=>{
                		if(response2.status === 200){
                            arrayOfStudent.push({...student  , career : response2.data.data.name})
						}

                        this.setState({
                            studentsAvailable : arrayOfStudent,
                            dataAvailableToSelect : arrayOfStudent
                        })
					})
				})

			}
		})

        axios.get('http://localhost:8000/teachers').then((response)=>{
            if(response.status === 200){
                console.log(response.data.data)
                this.setState({
                    teachersAvailable : response.data.data,
                })
            }
        })

        axios.get('http://localhost:8000/books').then((response)=>{
            if(response.status === 200){
                console.log(response.data.data)
                this.setState({
                    booksAvailable : response.data.data,
                })
            }
        })

	}


    onChange(dates, dateStrings) {
        console.log('From: ', dates[0], ', to: ', dates[1]);
        console.log('From: ', dateStrings[0], ', to: ', dateStrings[1]);

        this.setState({
			date_from : dateStrings[0],
			date_to : dateStrings[1]
		})
    }


    success(){
        Modal.success({
            title: '¡Orden registrado!',
            content: 'Se ha registrado el orden de libro',
            onOk() {
                window.location.reload()
            },
        });
    }

    handleOnSubmitButton(e){
    	e.preventDefault()
		console.log(this.state)
		if(this.state.date_from){
    		if(this.state.date_to){
    			if(this.state.bookSelected.id){
    				if(this.state.studentSelected){
						if(this.state.valueChoose === 0){
							axios.post('http://localhost:8000/orderBooks' , {
                                book : this.state.bookSelected.id,
                                student : this.state.studentSelected,
                                date_from : this.state.date_from,
                                date_final : this.state.date_to,
							}).then(response=>{
                                if(response.status === 200){
                                    this.success()
                                }
                            }).catch(error=>{
                            	console.log(error)
							})
						}else{
                            axios.post('http://localhost:8000/orderBooks' , {
                                book : this.state.bookSelected.id,
                                teacher : this.state.studentSelected,
                                date_from : this.state.date_from,
                                date_final : this.state.date_to,
                            }).then(response=>{
                                if(response.status === 200){
                                    this.success()
                                }
							}).catch(error=>{
                                console.log(error)
                            })
						}
					}else {
    					this.setError("Necesitamo un estudiante o profesor")
					}
				}else{
					this.setError("Necesitamos un libro")
				}
			}else{
				this.setError("Necesitamos la fecha de entrega")
			}
		}else{
			this.setError("Necesitamos la fecha")
		}
	}



	setError(message){
        Modal.error({
            title: 'Ocurrio un error',
            content: message,
        });
	}

    render() {

        const children = this.state.dataAvailableToSelect.map((student) => {
            return <OptionSecond key={student.id}>{student.name} - {student.career}</OptionSecond>
        })


        return (
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                <div className={"w-100 background-purple h-100"}>
                    <form className={"h-100"}>
                        <div className="form-login form-loginAlumno">
                            <p className="title-form title-formAlumno">Registro Prestamo</p>
                            <div className="formAlumno">
                         <FormControl className="hola">
                                <Select
                                    value={this.state.bookSelected.title}
                                    placeholder={"Selecciona tu libro"}
                                    /*className={"w-75"}*/
                                    onChange={(value)=>{
                                        console.log(value)
                                        this.setState({
                                            bookSelected : value
                                        })
                                    }}
                                >
                                    {
                                        this.state.booksAvailable.map(book=>{
                                            return(
                                                <Option value={book}>{book.title}</Option>
                                            )
                                        })
                                    }
                                </Select>
                        </FormControl>

                        
                        <FormControl className="hola">
                        <InputLabel htmlFor="custom-css-input" className="hello-hiPrestamo mt-3 mb-3">
                            Cantidad
                        </InputLabel>
                        <Input id="quantity" className="hello-RegisterPrestamo" type={"number"}/>
                    </FormControl>
                                <FormControl className="hola mt-3 mb-3">
                                    <RadioGroup  onChange={(e)=>{

                                    	if(e.target.value ===0){
                                    		this.setState({
                                                dataAvailableToSelect : this.state.studentsAvailable
											})
										}else {
                                            this.setState({
                                                dataAvailableToSelect : this.state.teachersAvailable
                                            })
										}

                                        this.setState({
                                            valueChoose: e.target.value,
                                        });
									}} value={this.state.valueChoose}>
                                        <Radio className="OptionsRegisterPrestamo" value={0}>Estudiante</Radio>
                                        <Radio className="OptionsRegisterPrestamo" value={1}>Profesor</Radio>
                                    </RadioGroup>
                                </FormControl>


                                <FormControl className="hola TextRegisterPrestamo mb-3 mt-3">
                                    <AutoComplete
                                       /* style={{width: "90%", marginTop: "1.0em", marginLeft: "0em", marginRight: "1.0em"}}*/
                                        onSelect={(item) => {
                                        	console.log(item)
                                        	this.setState({
												studentSelected : item
											})
                                        }}
                                        onSearch={(event)=>{
                                            console.log(this.state.data)
                                            let newFilterArray = this.state.dataAvailableToSelect.filter(item=>{
                                                return item.name.toLowerCase().search(event.toLowerCase()) !== -1;
                                            })

                                            this.setState({
                                                dataAvailableToSelect : newFilterArray,
                                            })

                                            if(event === ""){
                                            	if(this.state.valueChoose === 0){
                                                    this.setState({
                                                        dataAvailableToSelect:this.state.studentsAvailable,
                                                    })
												}else{
                                                    this.setState({
                                                        dataAvailableToSelect:this.state.teachersAvailable,
                                                    })
												}

                                            }
										}}
                                        size="large"
                                        placeholder="Busca tu alumno o profesor..."
                                    >

                                        {children}
                                    </AutoComplete>
                                </FormControl>



                                <FormControl className="hola DateRegisterPrestamo">
                                <RangePicker onChange={this.onChange}
    
                                />
                            </FormControl>
                            </div>
                        </div>
                        <div className={"w-100 text-center"}>
                            <Button variant="contained" className="btn-btn-form" onClick={this.handleOnSubmitButton}>
                                Registrar
                            </Button>
                        </div>
                    </form>
                </div>
                
            </div>
            
        )
    }
}

export default RegisterPrestamo