import React , {Component} from 'react'
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import purple from "@material-ui/core/colors/purple";
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import { Select  , Modal , AutoComplete} from 'antd';
import axios from 'axios'
import arrow from '../img/arrow.png';
import {NavLink} from 'react-router-dom'
const OptionSecond = AutoComplete.Option;


const Option = Select.Option;

const styles = theme => ({
    container: {
        display: "flex",
        flexWrap: "wrap",
    },
    margin: {
        margin: theme.spacing.unit,
    },
    textField: {
        flexBasis: 200,
    },
})

class RegisterEstante extends Component {
    constructor() {
        super()
        this.state = {
            careers : [],
            careersToShow : [],
            itemSelected : {},
            name : '',
            cara : '',
            levels : 0,
            errorName : false,
            errorCara : false,
        }

        this.handleSubmitForm= this.handleSubmitForm.bind(this)
        this.handleChangeForm= this.handleChangeForm.bind(this)
    }


    handleChangeForm(e){
        this.setState({[e.target.id] : e.target.value})
    }

    success(){
        Modal.success({
            title: '¡Estante registrado!',
            content: 'Se ha registrado exitosamente el estante',
            onOk() {
                window.location.reload()
            },
        });
    }

    handleSubmitForm(e){
        e.preventDefault()
        console.log(this.state)
        if(this.state.name){
            if(this.state.cara){
                if(this.state.itemSelected.id){
                    //Make post
                    axios.post("http://localhost:8000/estantes" , {
                        name : this.state.name,
                        level : this.state.levels,
                        career : this.state.itemSelected.id,
                        cara : this.state.cara
                    }).then(response=>{
                        if(response.status === 200){
                            this.success()
                        }
                        console.log(response)
                    }).catch(error=>{
                        console.log(error)
                    })
                }else {
                   //TODO : SEND A MESSAGE THAT WE NEED SOMEONE IN THE LIST
                }
            }else {
                this.setState({
                    errorCara : true
                })
            }
        }else{
            this.setState({
                errorName : true
            })
        }
    }


    componentDidMount(){
        axios.get('http://localhost:8000/careers').then(response=>{
            console.log(response.data.data)
            this.setState({
                careers : response.data.data,
                careersToShow : response.data.data
            })
        }).catch(error=>{
            console.log(error)
        })

	}

    render() {
        
        const children = this.state.careersToShow.map((career) => {
            return <OptionSecond key={career.id}>{career.name}</OptionSecond>
        })
        return (
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>

                <div className="container-fluid h-100">
                    <div className="w-100 background-purple h-100 justify-content-center mt-3">
                        <form className={"h-100 text-center"} onChange={this.handleChangeForm} onSubmit={this.handleSubmitForm}>
                            <p className="title-form title-formAlumno">Registro Estante</p>
                            <div className="formAlumno">
								<FormControl className="hola">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                        Nombre
                                    </InputLabel>
                                    <Input id="name" className="hello-entradaregisterEstante "/>
                                </FormControl>
                                <FormControl className="hola">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                        Cara
                                    </InputLabel>
                                    <Input id="cara" className="hello-entradaregisterEstante "/>
                                </FormControl>
                                <FormControl className="hola mb-3">
                                    <InputLabel htmlFor="custom-css-input" className="hello-hi">
                                        Nivel
                                    </InputLabel>
                                    <Input id="levels" className="hello-entradaregisterEstante " type={"number"}/>
                                </FormControl>
								<FormControl className="hola">
									{/*<Select
                                        value={this.state.itemSelected.name}
                                        placeholder={"Selecciona la carrera "}
                                        className="w-100 h-100"
                                        onChange={(value)=>{
                                            console.log(value)
                                            this.setState({
                                                itemSelected : value
                                            })
										}}
									>
                                        {
                                            this.state.careers.map(career=>{
                                                return(
                                                    <Option value={career}>{career.name}</Option>
                                                )
                                            })
                                        }
									</Select>*/}
                                    <AutoComplete
                                       /* style={{width: "90%", marginTop: "1.0em", marginLeft: "0em", marginRight: "1.0em"}}*/
                                        onSelect={(item) => {
                                        	console.log(item)
                                            this.setState({
                                                itemSelected : item
                                            })
                                        }}
                                        onSearch={(event)=>{
                                            console.log(this.state.data)
                                            let newFilterArray = this.state.careersToShow.filter(item=>{
                                                return item.name.toLowerCase().search(event.toLowerCase()) !== -1;
                                            })

                                            this.setState({
                                                careersToShow : newFilterArray,
                                            })

                                            if(event === ""){
                                                this.setState({
                                                    careersToShow: this.state.careers,
                                                })
                                            }
										}}
                                        size="large"
                                        placeholder="Busca la carrera"
                                    >

                                        {children}
                                    </AutoComplete>
                                </FormControl>
                            </div>
                            <div className="w-100 text-center">
                                <Button variant="contained" className="btn-btn-form" type={"submit"}>Registrar</Button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const formularioRegistroEstante = withStyles(styles)(RegisterEstante);
export default formularioRegistroEstante