import React , {Component} from 'react'
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.min.css';
import Avatar from '@material-ui/core/Avatar';
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import pdf from '../img/pdf.png';
import arrow from '../img/arrow.png';
import { DatePicker } from 'antd';
import { Checkbox, Row, Col } from 'antd';
import {NavLink} from 'react-router-dom'
import axios from "axios"
import { Table } from 'antd';
import { Popconfirm, message , Modal } from 'antd';
import TextField from '@material-ui/core/TextField';
import moment from 'moment'


function deleteSucess (){
    let context = this
    Modal.success({
        title: 'Libro eliminado',
        content: '',
        onOk() {
            window.location.reload()
          }, 
      });
}


const columns = [{
    title: 'Nombre',
    dataIndex: 'title',
}, {
    title: 'Editorial',
    dataIndex: 'publisher',
}, {
    title: 'Numero de edicion',
    dataIndex: 'num_of_edicion',
},
    {
        title: 'Autor',
        dataIndex: 'author',
    },
    {
        title: 'Nivel ubicado',
        dataIndex: 'level',
    },
    {
        title: 'Estante',
        dataIndex: 'estanteName',
    },
    {
        title: 'Cantidad',
        dataIndex: 'quantity',
    },
    ,{
        title: 'Funciones',
        dataIndex: 'id',
        key: 'id',
        render: text =>  <Popconfirm title="Estas seguro de eliminar este libro?" onConfirm={()=>{
            axios.delete('http://localhost:8000/books/'+ text)
            deleteSucess()
        }} okText="Eliminar" cancelText="No"><a href="#">Delete</a></Popconfirm>,
      },
];



class Books extends Component {
    constructor(){
        super()
        this.state={
            data : [],
            date : '',
        }
    }

    confirm(e) {
        console.log(e);
        message.success('Click on Yes');
    }


    componentDidMount(){
        moment.locale('en')
        let date = moment(Date()).format("LLLL")
        console.log(date)
        this.setState({date})
        let arrayOfEstantes = []
        axios.get('http://localhost:8000/books').then(response=>{
            if(response.status === 200){
                response.data.data.forEach(book=>{
                    arrayOfEstantes.push({...book, estanteName : book.estante.name})
                })

                this.setState({
                    data : arrayOfEstantes
                })
            }
        })
    }


    render(){
        return(
            <div className={"container-fluid"}>
                <div className="row w-100 mt-2">
                    <div className="col">
                        <AppBar color="default" className="position-relative">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    <NavLink to="/"><img className="arrow" src={arrow}/></NavLink>
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </div>
                </div>
                <div className="container-fluid col">
                    <div className="">
                        <div className="d-flex mt-3">
                            <div name="name" className="entrada-busqueda entrada-busqueda-books pt-2 text-center mt-2">
                                <h2>{this.state.date}</h2>
                            </div>
                            <Button variant="contained" className="btn-agendar-books text-white size-" onClick={() => {
                                this.props.history.push('/registerbooks')
                            }}><NavLink
                                to="/registerbooks" className="linkEstilos">Agregar</NavLink></Button>
                        </div>
                    </div>
                </div>
                <div className={"w-100 m-2"}>
                    <Table columns={columns} className="estilosTablaBooks" dataSource={this.state.data} bordered/>
                </div>
            </div>
        )
    }
}


export default Books