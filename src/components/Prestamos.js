import React , {Component} from 'react'
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.min.css';
import Avatar from '@material-ui/core/Avatar';
import  AppBar from '@material-ui/core/AppBar'
import  Toolbar from '@material-ui/core/Toolbar'
import  Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import pdf from '../img/pdf.png';
import arrow from '../img/arrow.png';
import { DatePicker } from 'antd';
import { Checkbox, Row, Col } from 'antd';




const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
});




const Prestamos = () =>(
    <div>
    <div className="container-fluid segundo-bloque col">
          <AppBar position="static" color="default" className="barra-segundoBloque">
                <Toolbar>
                <Typography variant="title" color="inherit">
                    <a href="#"><img className="arrow" src={arrow} /></a>
                </Typography>
                </Toolbar>
            </AppBar>
            </div>
          


           <div className="container-fluid tercer-bloque col-md-9">
            <div className="">
            <div className="d-flex">
              
                <div name="name" className="entrada-busqueda entrada-busqueda-books text-center pt-3">{Date()}</div>


                
                <Button variant="contained" className="btn-agendar-books text-white size-">Agregar</Button>
            </div>
            
          </div>
        </div>


        <div className="container-fluid cuarto-bloque col">
        <table class="table table-bordered table-dark">
        <thead>
            <tr>
            <th scope="col" className="table-color">ID</th>
            <th scope="col" className="table-color">Nombre</th>
            <th scope="col" className="table-color">Autor</th>
            <th scope="col" className="table-color">Estado</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
             </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            
            </tr>
            
        </tbody>
        </table>
        </div>

        <div className="d-flex-inline quinto-bloque ">
        <Button variant="contained" className="btn-atras">
        Atras
        </Button>
        <Button variant="contained" className="btn-siguiente float-right">
        Siguiente
        </Button>
</div>
</div>
)



export default Prestamos